# coding=utf-8
import re
import sqlite3
from aiogram import Bot, Dispatcher, types, executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.utils import exceptions
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import ParseMode, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from settings import *
from sql import *
import asyncio
import json


answ_uid = 0

bot = Bot(token=tg_token)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

def_keyboard = [[types.KeyboardButton(text="Мои вишлисты")]]

@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    uid = message.from_user.id
    tg_username = message.from_user.username

    await sql_check_user(uid, tg_username)

    if uid < 0: return
    print('start', uid)

    t = 'Привет!\nЭто бот для вишлистов\n\n'

    rm_main = types.ReplyKeyboardMarkup(keyboard=def_keyboard, resize_keyboard=True)
    await bot.send_message(chat_id=uid, text=t, reply_markup=rm_main)


@dp.message_handler(commands=['ping'])
async def process_ping_command(message: types.Message):
    await message.answer('pong')


async def my_wishlists(uid):
    wishlists = await sql_get_user_wishlists(uid)

    if wishlists:
        rm = types.InlineKeyboardMarkup()
        t_out = "Твои вишлисты:\n"

        for wl in wishlists:
            rm.add(types.InlineKeyboardButton(text=wl["wname"], callback_data=f"show_wishlist:{wl['wid']}"))
            t_out += f"{wl['wname']}\n"
        rm.add(types.InlineKeyboardButton(text="Новый вишлист", callback_data="new_wishlist"))
    else:
        t_out = "У тебя нет вишлистов. Создадим новый?"
        rm = types.InlineKeyboardMarkup()
        rm.add(types.InlineKeyboardButton(text="Новый вишлист", callback_data="new_wishlist"))

    return t_out, rm


async def get_wishlist(wid):
    items = await sql_get_wishlist_items(wid)
    t = ''
    if len(items):
        j = 0
        for i in items:
            j += 1
            if i["itlink"] is not None:
                t += f"{j}) {i['itname']} <a href=\"{i['itlink']}\">ссылка</a>\n"
            else:
                t += f"{j}) {i['itname']}\n"
    else:
        t = 'Ой, тут пока пусто'
    return t, items

# =======================================================================
@dp.callback_query_handler()
async def process_callback(callback_query: types.CallbackQuery):
    uid = callback_query.from_user.id
    mid = callback_query.message.message_id
    qd = callback_query.data
    print('Q|', uid, qd)

    if 'answ' in qd:
        answ_uid = qd.split(':')[1]

        t = f'Пишем текст для {answ_uid}'
        await bot.send_message(chat_id=admin_id, text=t)


    if 'new_wishlist' in qd:
        await sql_set_state(uid, 'new_wishlist_name')
        t = f'Отлично!\n\nКак назовем наш новый вишлист?'
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=None)


    if 'del_wishlist1' in qd:
        wid = qd.split(':')[1]
        t = f'Хочешь удалить вишлист {await sql_get_wishlist_name(wid)}?'
        rm = types.InlineKeyboardMarkup()
        rm.add(types.InlineKeyboardButton(text='Да, удаляем!', callback_data=f"del_wishlist2:{wid}"))
        rm.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f"my_wishlists"))
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=rm)


    if 'del_wishlist2' in qd:
        wid = qd.split(':')[1]
        await sql_del_wishlist(wid)
        t = 'Вишлист удален'
        rm = types.InlineKeyboardMarkup()
        rm.add(types.InlineKeyboardButton(text='⬅️ К вишлистам', callback_data=f"my_wishlists"))
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=None)


    if 'del_some_item' in qd:
        wid = qd.split(':')[1]
        t = f'Хочешь удалить что-то из вишлиста?\n\nВыбери номер.\nОсторожно, удаление произойдет сразу!\n\n'

        Wishlist = await get_wishlist(wid)
        t += Wishlist[0]
        wishlist_items = Wishlist[1]

        # Create InlineKeyboardMarkup
        rm = types.InlineKeyboardMarkup()

        # Generate buttons for each item
        buttons = []
        for index, item in enumerate(wishlist_items):
            buttons.append(
                types.InlineKeyboardButton(text=str(index + 1), callback_data=f'delete_item:{wid}:{index + 1}'))

        # Add buttons in rows of 4
        for i in range(0, len(buttons), 4):
            rm.row(*buttons[i:i + 4])

        rm.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f"show_wishlist:{wid}"))
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=rm, parse_mode=html)


    if 'del_item' in qd:
        wid = qd.split(':')[1]
        itid = qd.split(':')[2]
        await sql_del_item(itid)
        t = f'Удалил'
        rm = types.InlineKeyboardMarkup()
        rm.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f"show_wishlist:{wid}"))
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=rm, parse_mode=html)


    if 'my_wishlists' in qd:
        await bot.edit_message_reply_markup(uid, mid, reply_markup=None)
        t, rm = await my_wishlists(uid)
        await bot.send_message(chat_id=uid, text=t, reply_markup=rm)

    if 'new_item:' in qd:
        wid = qd.split(':')[1]
        t = ("Напиши, что ты хочешь\n\nОдним словом или длинным текстом, но если у тебя есть ссылка, то лучше "
             "добавить её на следующем шаге.")
        await sql_set_state(uid, f'new_item:{wid}')
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=None)

    if 'show_wishlist:' in qd:
        wid = qd.split(':')[1]
        t = f"Вишлист {await sql_get_wishlist_name(wid)}\n\n"
        w = await get_wishlist(wid)
        t += w[0]
        rm = types.InlineKeyboardMarkup()
        rm.add(types.InlineKeyboardButton(text='Добавить желание', callback_data=f"new_item:{wid}"))
        rm.add(types.InlineKeyboardButton(text='Убрать желание', callback_data=f"del_some_item:{wid}"))
        rm.add(types.InlineKeyboardButton(text='Удалить вишлист', callback_data=f"del_wishlist1:{wid}"))
        rm.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f"my_wishlists"))
        await bot.edit_message_text(text=t, chat_id=uid, message_id=mid, reply_markup=rm, parse_mode=html)



    await callback_query.answer()



# =======================================================================


@dp.message_handler(content_types="text")
async def echo(message: types.Message):
    uid = message.from_user.id
    tg_username = message.from_user.username
    mid = message.message_id
    st = await sql_get_state(uid)

    print(f"U| {uid} {tg_username if tg_username else ''}, <<<{message.text}>>>")

    if uid < 0:
        return

    global answ_uid

    t_out = ''
    t_in = message.text
    rm = None

    if t_in == "300":
        t_out = "Серьёзно?"

    elif t_in == "Мои вишлисты":
        await sql_set_state(uid, '')
        t_out, rm = await my_wishlists(uid)


    elif st == "new_wishlist_name":
        wid = await sql_create_new_wishlist(t_in, uid)
        t_out = 'Ура, вишлист создан!\n\nПока он пустой, добавим в него что-нибудь?'
        rm = types.InlineKeyboardMarkup()
        rm.add(types.InlineKeyboardButton(text='Да!', callback_data=f"new_item:{wid}"))
        rm.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f"my_wishlists"))

    elif "new_item:" in st:
        wid = st.split(':')[1]
        itid = await sql_add_item_to_wishlist(wid, t_in)
        t_out = ("Классная идея!\n\n"
                 "У тебя есть ссылка? Отправь ее мне или нажми <b>нет</b>")
        no_keyboard = [[types.KeyboardButton(text="Нет")]]
        await sql_set_state(uid, f'waiting_for_link:{itid}')
        rm = types.ReplyKeyboardMarkup(keyboard=no_keyboard, resize_keyboard=True)

    elif "waiting_for_link:" in st:
        itid = st.split(':')[1]
        rm = types.InlineKeyboardMarkup()
        if t_in.lower() != "нет":
            await sql_update_item_link(itid, t_in)
            t1 = 'Записал!'
        else:
            t1 = 'Хорошо, ты сможешь добавить ее позже'

        t_out = "Добавим еще что-нибудь в вишлист или вернемся назад?"

        rm_main = types.ReplyKeyboardMarkup(keyboard=def_keyboard, resize_keyboard=True)
        await bot.send_message(chat_id=uid, text=t1, reply_markup=rm_main, parse_mode=html)

        await asyncio.sleep(DELAY_TYPING)

        wid = await sql_get_wid_of_item(itid)
        rm.add(types.InlineKeyboardButton(text='Да!', callback_data=f"new_item:{wid}"))
        rm.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f"my_wishlists"))


    else:
        t_out = 'Не знаю такой команды и не жду сейчас от тебя текста'

    if t_out:
        if rm:
            await bot.send_message(chat_id=uid, text=t_out, reply_markup=rm, parse_mode=html)
        else:
            await bot.send_message(chat_id=uid, text=t_out, parse_mode=html)


@dp.message_handler(content_types=['photo', 'video', 'document'])
async def handle_docs_photo(message):
    uid = message.from_user.id
    mid = message.message_id
    username = message.from_user.username
    print(f"Медиа от @{username} {uid}")


async def on_startup(dp):
    # scheduler.add_job(pingdb, "interval", minutes=60, args=(bot,))
    pass

if __name__ == '__main__':
    print('start', botname)

    from aiogram import executor

    # with suppress(exceptions.NetworkError):
    #     await executor.start_polling(dp, on_startup=on_startup)
    executor.start_polling(dp, on_startup=on_startup)