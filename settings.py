import logging
import random
import asyncio
# import asyncpg
import tzlocal
from apscheduler.schedulers.asyncio import AsyncIOScheduler
import aiosqlite



# test = 1
test = 0

running_on_server = 1

tz_system = tzlocal.get_localzone()
print('System TZ', tz_system)
scheduler = AsyncIOScheduler(timezone=str(tz_system))
scheduler.start()

if test:
    tg_token = "328682751:AAHDnN7m4j_Ur_9Nb9DNGi-exuy2hQhpXZ4"
    botname = 'cami_buddy_bot'
else:
    tg_token = "7260274816:AAFf1LEikV0Yh7JaPkVUaf42wGgO4wZwtOk"
    botname = 'you_wishlist_bot'


admin_list = ['182116723']
my_id = 182116723
admin_id = admin_list[0]
testgroupid = "-1001527035014"  # тестовая Боты

db_local = 'wish.db'


logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)

typing = 'TYPING'
html = 'HTML'
DELAY_TYPING = 1

async def status_typing(bot, uid, time=DELAY_TYPING):
    await bot.send_chat_action(chat_id=uid, action=typing)
    await asyncio.sleep(time)


class AsyncPGManager:
    def __init__(self, dbname, user, password, host='localhost', min_size=1, max_size=10):
        self.dbname = dbname
        self.user = user
        self.password = password
        self.host = host
        self.min_size = min_size
        self.max_size = max_size
        self.pool = None

    async def create_pool(self):
        self.pool = await asyncpg.create_pool(
            user=self.user,
            password=self.password,
            database=self.dbname,
            host=self.host,
            min_size=self.min_size,
            max_size=self.max_size
        )

    async def close(self):
        await self.pool.close()

    async def execute(self, sql, *params):
        async with self.pool.acquire() as connection:
            try:
                async with connection.transaction():
                    await connection.execute(sql, *params)
            except asyncpg.PostgresError as e:
                print(f"Error executing query: {e}, request: {sql}")
                raise

    async def fetch(self, sql, *params):
        async with self.pool.acquire() as connection:
            try:
                return await connection.fetch(sql, *params)
            except asyncpg.PostgresError as e:
                print(f"Error fetching: {e}, request: {sql}")
                raise

    async def fetchrow(self, sql, *params):
        async with self.pool.acquire() as connection:
            try:
                result = await connection.fetchrow(sql, *params)
                return result
            except asyncpg.PostgresError as e:
                print(f"Error fetching row: {e}, request: {sql}, params: {params}")
                raise

    async def fetch(self, sql, *params):
        async with self.pool.acquire() as connection:
            try:
                return await connection.fetch(sql, *params)
            except asyncpg.PostgresError as e:
                print(f"Error fetching value: {e}, request: {sql}")
                raise


db_params = {
    'dbname': 'santa_db',
    'user': 'postgres',
    'password': 'cam!31415926',
    'host': 'localhost' if running_on_server else '45.153.184.82'
}
pg_manager = AsyncPGManager(**db_params)



class AsyncSQLiteManager:
    def __init__(self, db_path):
        self.db_path = db_path

    async def execute(self, sql, *params):
        async with aiosqlite.connect(self.db_path) as db:
            try:
                await db.execute(sql, params)
                await db.commit()
            except aiosqlite.Error as e:
                print(f"Error executing query: {e}, request: {sql}")
                raise

    async def fetch(self, sql, *params):
        async with aiosqlite.connect(self.db_path) as db:
            try:
                cursor = await db.execute(sql, params)
                rows = await cursor.fetchall()
                return rows
            except aiosqlite.Error as e:
                print(f"Error fetching: {e}, request: {sql}")
                raise

    async def fetchrow(self, sql, *params):
        async with aiosqlite.connect(self.db_path) as db:
            try:
                cursor = await db.execute(sql, params)
                row = await cursor.fetchone()
                return row
            except aiosqlite.Error as e:
                print(f"Error fetching row: {e}, request: {sql}, params: {params}")
                raise

db_params_lite = {
    'db_path': 'wish.db'
}
sqlite_manager = AsyncSQLiteManager(**db_params_lite)