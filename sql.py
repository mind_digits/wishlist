from settings import *
import sqlite3


# def sql_add_user_to_db(uid, tg_username):
#     cursor.execute("""INSERT INTO users VALUES (?,?)""", (id, name))
#     conn.commit()
#     print(f"User {uid} {tg_username} added to database.")

async def sql_check_user(uid, tg_username):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    sql = "SELECT * FROM users WHERE uid = ?"
    cursor.execute(sql, (uid,))
    a = cursor.fetchall()
    if len(a) == 0:
        print('Новенький, добавляю', tg_username, uid)
        cursor.execute("""INSERT INTO users VALUES (?,?)""", (int(uid), tg_username, ))
        conn.commit()

async def sql_get_user_wishlists(uid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    sql = "SELECT wid, wname FROM wishlists WHERE uid = ?"
    cursor.execute(sql, (uid,))
    wishlists = cursor.fetchall()
    conn.close()
    return [{"wid": wid, "wname": wname} for wid, wname in wishlists]


async def sql_create_new_wishlist(wname, uid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()

    # Inserting new wishlist with auto-incremented wid
    cursor.execute("INSERT INTO wishlists (wname, uid) VALUES (?, ?)", (wname, uid))
    conn.commit()

    # Retrieve the last inserted wid
    new_wid = cursor.lastrowid

    conn.close()
    return new_wid

async def sql_del_wishlist(wid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    cursor.execute("DELETE FROM wishlists WHERE wid = ?", (wid,))
    conn.commit()
    conn.close()

async def sql_get_state(uid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    sql = "SELECT st FROM users WHERE uid = ?"
    cursor.execute(sql, (uid,))
    res = cursor.fetchall()
    conn.close()
    return res[0][0]


async def sql_set_state(uid, st):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    sql = "UPDATE users SET st = ? WHERE uid = ?"
    cursor.execute(sql, (st, uid))
    conn.commit()
    conn.close()


async def sql_get_wishlist_items(wid):
    conn = sqlite3.connect(db_local)  # Replace with your database file
    cursor = conn.cursor()

    query = """
    SELECT itname, itlink
    FROM items
    WHERE wid = ?
    """

    cursor.execute(query, (wid,))
    rows = cursor.fetchall()
    conn.close()

    items = [{"itname": row[0], "itlink": row[1]} for row in rows]

    return items

async def sql_add_item_to_wishlist(wid, itname):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()

    # Добавление нового элемента в вишлист
    cursor.execute("INSERT INTO items (wid, itname) VALUES (?, ?)", (wid, itname))
    conn.commit()

    # Получение последнего вставленного itid
    itid = cursor.lastrowid

    conn.close()
    return itid


async def sql_del_item(itid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()

    # Удаление элемента по itid
    cursor.execute("DELETE FROM items WHERE itid = ?", (itid,))
    conn.commit()

    conn.close()


async def sql_update_item_link(itid, link):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()

    # Обновление ссылки для элемента
    cursor.execute("UPDATE items SET itlink = ? WHERE itid = ?", (link, itid))
    conn.commit()

    conn.close()


async def sql_book_item(itid, taken_uid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()

    # Обновление статуса taken и taken_uid
    cursor.execute("UPDATE items SET taken = TRUE, taken_uid = ? WHERE itid = ?", (taken_uid, itid))
    conn.commit()

    conn.close()


async def sql_release_item(itid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()

    # Обновление статуса taken и taken_uid
    cursor.execute("UPDATE items SET taken = FALSE, taken_uid = NULL WHERE itid = ?", (itid,))
    conn.commit()

    conn.close()


async def sql_get_wid_of_item(itid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    sql = "SELECT wid FROM items WHERE itid = ?"
    cursor.execute(sql, (itid,))
    res = cursor.fetchall()
    conn.close()
    return res[0][0]


async def sql_get_wishlist_name(wid):
    conn = sqlite3.connect(db_local)
    cursor = conn.cursor()
    sql = "SELECT wname FROM wishlists WHERE wid = ?"
    cursor.execute(sql, (wid,))
    res = cursor.fetchall()
    conn.close()
    return res[0][0]